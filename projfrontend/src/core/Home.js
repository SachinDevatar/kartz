import React from 'react'
import "../styles.css"
import { API } from "../backend";
import Base from './Base';




export default function Home() { // class is reserved in react so className!
    // console.log("API IS", process.env.REACT_APP_BACKEND); since it's in backend folder
    console.log("API IS", API);

    return (
        <Base title="Home Page" description="Welcome to the kArtZ Store" >
            <div className="row">
                <div className="col-4">
                    <button className='btn btn-success'>TEST</button>
                </div>
                <div className="col-4">
                    <button className='btn btn-success'>TEST</button>
                </div>
                <div className="col-4">
                    <button className='btn btn-success'>TEST</button>
                </div>
            </div>
        </Base>
    )
}

// majorly depends on index.js for showing up our application

import React from "react"
import Routes from "./Routes"
import ReactDom from "react-dom" // default, using it we can render router

ReactDom.render(<Routes />, document.getElementById("root")); // route is inside public in index.js that's the root looking for


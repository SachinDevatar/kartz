import { API } from "../../backend";
// API Means API = process.env.REACT_APP_BACKEND;
// API Meaning REACT_APP_BACKEND=http://localhost:8000/api/
//https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch

// fetch request being sent
export const signup = user => {
    return fetch(`${API}/signup`, {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
        },
        body: JSON.stringify(user)
    })
        .then(response => {
            return response.json();
        })
        .catch(err => console.log(err));
}

export const signin = user => {
    return fetch(`${API}/signin`, { // Hit the back end
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
        },
        body: JSON.stringify(user)
    })
        .then(response => {
            return response.json();
        })
        .catch(err => console.log(err));
};

// token is set to check if the user is succesfully signin
export const authenticate = (data, next) => {
    if (typeof window !== "undefined") {
        localStorage.setItem("jwt", JSON.stringify(data))
        next();
    }
}

// remove jwt
export const signout = next => { // it's a middle ware next allows the call back
    if (typeof window !== "undefined") {
        localStorage.removeItem("jwt")
        next();

        return fetch(`{API}/signout`, {
            method: "GET"
        })
            .then(resonse => console.log("signout success"))
            .catch(err => console.log(err))
    }
};

export const isAutheticate = () => {
    if (typeof window == "undefined") {
        return false
    }
    if (localStorage.getItem("jwt")) {
        return JSON.parse(localStorage.getItem("jwt"));
    } else {
        return false;
    }
};


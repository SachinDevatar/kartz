import React, { useState } from "react"
import Base from "../core/Base"
import { Link } from "react-router-dom";
import { signup } from "../auth/helper";

const Signup = () => {

    //https://reactjs.org/docs/hooks-state.html
    //usestate or the part of values call by values.email ..
    const [values, setValues] = useState({
        name: "",
        email: "",
        password: "",
        error: "",
        success: false
    });

    // destructure since avoid using of values.name, values.email ...
    const { name, email, password, error, success } = values

    // advance functional programming treat as amazing citizens or first level
    const handleChange = name => event => {
        setValues({ ...values, error: false, [name]: event.target.value });

    };

    const onSubmit = event => {
        event.preventDefault()
        setValues({ ...values, error: false })
        signup({ name, email, password }) // destructure since avoid using of values.name, values.email ...
            .then(data => {
                if (data.error) {
                    setValues({ ...values, error: data.error, success: false })
                } else {
                    setValues({
                        ...values,
                        name: "", // since we need to clear the input field
                        email: "",
                        password: "",
                        error: "",
                        success: true
                    });
                }
            })
            .catch(console.log("Error in sign"))
    }


    const signUpForm = () => {
        return (
            <div className="row">
                <div className="col-md-6 offset-sm-3 text-left">
                    <form>
                        <div className="form-group">
                            <lable className="text-light"> Name</lable>
                            <input className="form-control" onChange={handleChange("name")} value={name} type="text" />
                        </div>
                        <div className="form-group">
                            <lable className="text-light"> Email</lable>
                            <input className="form-control" onChange={handleChange("email")} value={email} type="email" />
                        </div>
                        <div className="form-group">
                            <lable className="text-light"> Password</lable>
                            <input onChange={handleChange("password")} className="form-control" value={password} type="text" /> {/* will keep align proper */}
                        </div>
                        <button onClick={onSubmit} className="btn btn-success btn-block">Submit </button>
                    </form>
                </div>
            </div>

        )
    };

    const successMessage = () => {
        return (
            <div className="row">
                <div className="col-md-6 offset-sm-3 text-left">

                    <div className="alert alert-success"
                        style={{ display: success ? "" : "none" }}>

                        New account was created successfully. Please <Link to="/signin"> Login Here</Link>
                    </div>
                </div>
            </div>

        );
    };

    const errorMessage = () => {
        return (

            <div className="row">
                <div className="col-md-6 offset-sm-3 text-left">

                    <div className="alert alert-danger"
                        style={{ display: error ? "" : "none" }}>

                        {error}
                    </div>
                </div>
            </div>
        );
    };


    return (
        <Base title="Sign up page" description="A page for user to sign up!">
            {/* <h1>Sign up works</h1>  */}
            {successMessage()}
            {errorMessage()}
            {signUpForm()}
            <p className="text-white text-center">{JSON.stringify(values)}</p>
        </Base>
    );
};

export default Signup;




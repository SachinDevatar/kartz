const express = require("express");

const app = express();

const port = 8001;
// {} need to return so using ()


app.get("/home", (req, res) => {
    return res.send("Home Page");
});

// app.get("/", (req, res) => {
//     return res.send("Home Page");
// });


const admin = (req, res) => {
    return res.send("this is admin dashboard");
}; // () => call back function

const isAdmin = (req, res, next) => { // when you write a middle ware update next here as well
    console.log("isAdmin is running");
    next();
}

const isLoggedIn = (req, res, next) => {
    console.log("isLoggedIn is coming to play");
    next();
}



app.get("/admin", isLoggedIn, isAdmin, admin) // isAdmin here is a middle ware and it should define as above



app.get("/login", (req, res) => {
    return res.send("Visiting Login Route");
});

app.get("/Signup", (req, res) => {
    return res.send(" You are Signup!!");
});

app.get("/Signout", (req, res) => {
    return res.send("Sorry You are Signout!!");
});


app.listen(port, () => {
    console.log("Server is running....")
});

// const port = 3000

// app.get('/', (req, res) => res.send('Hello World!'))

// app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))
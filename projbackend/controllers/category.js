const Category = require("../models/category");

// it will get particular id of customer through params
exports.getCategoryById = (req, res, next, id) => {

    Category.findById(id).exec((err, cate) => {
        if (err) {
            return res.status(400).json({
                error: "Category not found in DB"
            });
        }
        req.category = cate;
        next();
    });

};

exports.createCategory = (req, res) => {
    // we are creating a new object category! beacause it's comming from Category (models see last module line)
    // I cannot use it directly.
    const category = new Category(req.body); // populate in req.body which is comming from category.js from models
    category.save((err, category) => {
        if (err) {
            return res.status(400).json({
                error: "Not able to save category in DB"
            });
        }
        res.json({ category });
    });
};


exports.getCategory = (req, res) => {
    return res.json(req.category);
};



exports.getAllCategory = (req, res) => {
    Category.find().exec((err, categories) => {
        if (err) {
            return res.status(400).json({
                error: "No Categories found"
            });
        }
        res.json(categories);
    });
};


exports.updateCategory = (req, res) => {
    const category = req.category;
    category.name = req.body.name;

    category.save((err, updatedCategory) => {
        if (err) {
            return res.status(400).json({
                error: "Failed to update category"
            });
        }
        res.json(updatedCategory);
    });
};


exports.removeCategory = (req, res) => {
    const category = req.category; // extracting the info from the parameter which is a middleware

    category.remove((err, category) => { // here the category which is now deleted
        if (err) {
            return res.status(400).json({
                error: "Failed to delete this `category` category"
            });
        }
        res.json({
            message: "Successfully deleted "
        });
    });

};
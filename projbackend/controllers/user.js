/* eslint-disable no-unused-vars */
const User = require("../models/user");
const Order = require("../models/order");

// params 
// it will get particular id of customer through params
exports.getUserById = (req, res, next, id) => {
    // find user by id in mongoDB
    //try to get the user details from the User model and attach it to the request object(req.profile)
    User.findById(id).exec((err, user) => {
        // first always for err
        if (err || !user) {
            return res.status(400).json({
                error: "No user was found in DB"
            });
        }
        req.profile = user; // we got a particular profile of user remeber that coggle siudfjsfaoisd in url
        next();
    });
};

exports.getUser = (req, res) => {
    //TODO: get back here for password



    // making undefined salt and encrypassword not making undefined in database we are doing undefined in userprofile only
    req.profile.salt = undefined;  // " " use this it will show empty or use undefined which won't show at all
    req.profile.encry_password = undefined;
    req.profile.createdAt = undefined;
    req.profile.updatedAt = undefined;


    return res.json(req.profile);
};

// whatever we are doing it that's already in database see 07 in 8th section at 4:20
exports.updateUser = (req, res) => {
    User.findByIdAndUpdate( // find the user
        { _id: req.profile._id }, // middleware fire userID and poplulate a field called req.profile and set it
        { $set: req.body }, // where frontend kicks in And what really need to update
        { new: true, useFindAndModify: false }, // new is true since updation is going on
        (err, user) => { // receiving a user so in 45, 46 we used user
            if (err) {
                return res.status(400).json({
                    error: "You are not authorized to update this user"
                });
            }
            user.salt = undefined;  // " " use this it will show empty or use undefined which won't show at all
            user.encry_password = undefined;
            res.json(user);
        }
    );

};

exports.userPurchaseList = (req, res) => {
    Order.find({ user: req.profile._id })
        .populate("user", "_id name") // if you want you can email as well no comma nothing name email
        .exec((err, order) => {
            if (err) {
                return res.status(400).json({
                    error: "No Order in this account"
                });
            }
            return res.json(order); // gives back transcation_id, amount, address
        });

};

exports.pushOrderInPurchaseList = (req, res, next) => {

    let purchases = [];
    req.body.order.products.forEach(product => {
        purchases.push({
            _id: product._id,
            name: product.name,
            description: product.description,
            category: product.category,
            quantity: product.quantitiy,
            amount: req.body.order.amount,
            transaction_id: req.body.order.transaction_id

        });
    });

    // store this in DB
    User.findOneAndUpdate(
        { _id: req.profile._id },
        { $push: { purchases: purchases } },
        { new: true }, // from DB whatever update send the object which is updated one
        (err, purchase) => {
            if (err) {
                return res.status(400).json({
                    error: "unable to save purchase list"
                });
            }
            next();
        }
    );

};








// to get all user data from DB 
// exports.getAllUsers = (req, res) => {
//     User.find().exec((err, users) => {
//         if (err || !users) {
//             return res.status(400).json({
//                 error: "NO user found"
//             });
//         }
//         res.json(users)
//     });
// };
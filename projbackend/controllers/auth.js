/* eslint-disable no-unused-vars */
/* eslint-disable quotes */
const User = require("../models/user"); // will bring our user
const { check, validationResult } = require('express-validator'); // here only validationResults are used
var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');


exports.signup = (req, res) => {
    // // console.log("Signup works!");
    // console.log("REQ BODY", req.body);
    // //POST Request
    // res.json({
    //     message: "signup route works!"
    // });

    const errors = validationResult(req);

    if (!errors.isEmpty()) // if error is not empty
    {
        return res.status(422).json({
            error: errors.array()[0].msg
        });
    }

    const user = new User(req.body); // small user has been a object from class of USER

    // user is a object of User so that User is in mongoDB schema with this we will get name, email all from schema that why used user 

    // cannot use User directly because we are in other file so we will use this convention method!
    user.save((err, user) => { // error and user (object saved in the database)
        if (err) {
            return res.status(400).json({
                err: " NOT ABLE TO SAVE USER IN DB"
            });
        }
        // res.json(user) it will all the fields which is present in the schema so in the next line I am destructing it.
        res.json({
            name: user.name,
            email: user.email,
            id: user._id // _id I am getting in DB info need to throw to the user
        });
    }); // we can use populate as well or save so it's saving in MongoDB and giving resonse 
    //back with call back ()=>
};


exports.signin = (req, res) => {
    const errors = validationResult(req);
    const { email, password } = req.body; // extract email and password from req.body

    if (!errors.isEmpty()) // if error is not empty
    {
        return res.status(422).json({
            error: errors.array()[0].msg
        });
    }
    // findone using mongoDB to check whether exists or not!
    User.findOne({ email }, (err, user) => { // find the very first one in the database very aggressive in finding email
        // () => in call back it will return a positive value or error that is user or err
        if (err || !user) {
            return res.status(400).json({
                error: "USER email does not exists"
            });
        }

        if ((!user.autheticate(password))) {
            return res.status(401).json({
                error: "Email and Password do not match"
            });
        }

        //CREATE TOKEN
        const token = jwt.sign({ _id: user._id }, process.env.SECRET); // token is created based on ID
        // put token in cookie
        res.cookie("token", token, { expire: new Date() + 9999 });// token created and expire here we gave expire a long time
        // so that it will store long in cookie in real world you need to shrink the date bit less


        // send response to front end
        const { _id, name, email, role } = user; // do deconstruction by pulling out user info that is id, name, email and role
        // role let you know user is admin or not
        return res.json({ token, user: { _id, name, email, role } });

    });



};



exports.signout = (req, res) => {
    res.clearCookie("token");
    // res.send("user signout success");
    res.json({ // Json response
        message: "User signout Sucessfully"
    });
};

// protected routes
exports.isSignedIn = expressJwt({
    secret: process.env.SECRET,
    userProperty: "auth" // here auth will validate the id created when signin and also in database
});



// custome middleware

exports.isAuthenticated = (req, res, next) => {

    let checker = req.profile && req.auth && req.profile._id == req.auth._id; // not === we are not checking for object
    // are checking for only values so ==
    // property profile only set when user is logged in 
    if (!checker) {
        return res.status(403).json({
            error: " ACCESS DENIED"
        });
    }


    next();
};

exports.isAdmin = (req, res, next) => {
    if (req.profile.role === 0) { // profile set from front end 0 indicates regular user see in salt at user.js
        return res.status(403).json({
            error: "You are not ADMIN, Access denied"
        });
    }


    next();
};
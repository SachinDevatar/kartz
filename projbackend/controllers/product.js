const Product = require("../models/product");
const formidable = require("formidable");
const _ = require("lodash"); // helps in creating and working new object and arrays stuff like that
const fs = require("fs"); // file system which comes default!
// eslint-disable-next-line no-unused-vars
const { formatWithOptions } = require("util");



exports.getProductById = (req, res, next, id) => {
    Product.findById(id)
        .populate("category") // we are populating based on category
        .exec((err, product) => {
            if (err) {
                return res.status(400).json({
                    error: "Product not found"
                });
            }

            req.product = product;
            next();
        });
};

exports.createProduct = (req, res) => {
    let form = new formidable.IncomingForm(); // has a object of form
    // expects 3 fields - err, field(name description and price) and files 
    form.keepExtensions = true; // files in jpeg or peg
    form.parse(req, (err, fields, file) => {
        if (err) {
            return res.status(400).json({
                error: "Problem with image, Please Check agian"
            });
        }

        // destructure the fields
        //fields.price
        const { name, description, price, category, stock } = fields;

        if (
            !name ||
            !description ||
            !price ||
            !category ||
            !stock
        ) {
            return res.status(400).json({
                error: "Please include all the fields"
            });
        }

        // TODO: restrictions on field (Created above)
        let product = new Product(fields);



        // Handle file here
        if (file.photo) {
            if (file.photo.size > 3000000) // checking with photo size that is 3 mb 1024*1024 *2
            {
                return res.status(400).json({
                    error: "File size too big!"
                });
            }
            // data came from product in models mention fullpath of our file and passing to form that is parameter which is in 28 line
            product.photo.data = fs.readFileSync(file.photo.path); // technically this two lines saves in database
            product.photo.contentType = file.photo.type; //10 Section - 03 @12:28 type jpeg or peg
        }
        // console.log(product);


        // Save to the DB
        product.save((err, product) => {
            if (err) {
                res.status(400).json({
                    error: "Saving tshirt in DB failed"
                });
            }
            res.json(product);
        });
    });
};

// get a single product
exports.getProduct = (req, res) => {
    req.product.photo = undefined; // mp3 will be bulky to get it will take time and effect the performance so undefined
    return res.json(req.product);// so quick since photo is undefined here
};


// middleware since photo will load load in the background (so middleware) once the get request is made from the front end which is the above
exports.photo = (req, res, next) => {
    if (req.product.photo.data) { // safe chaining 
        res.set("Content-Type", req.product.photo.contentType);
        return res.send(req.product.photo.data);
    }
    next();
};


// delete controllers
exports.deleteProduct = (req, res) => {
    let product = req.product;
    product.remove((err, deletedProduct) => {
        if (err) {
            return res.status(400).json({
                error: "Failed to delete the product"
            });
        }
        res.json({
            message: "Deletion was a success",
            deletedProduct
        });
    });
};



// update controllers see  10 @08 @7:40 about the plan 
exports.updateProduct = (req, res) => {
    let form = new formidable.IncomingForm(); // has a object of form
    // expects 3 fields - err, field(name description and price) and files 
    form.keepExtensions = true; // files in jpeg or peg
    form.parse(req, (err, fields, file) => {
        if (err) {
            return res.status(400).json({
                error: "Problem with image, Please Check agian"
            });
        }



        // updation of code
        let product = req.product;
        product = _.extend(product, fields);// take existing value in the object which is having and 
        //it extend the value I mean all the updation value involve there.



        // Handle file here
        if (file.photo) {
            if (file.photo.size > 3000000) // checking with photo size that is 3 mb 1024*1024 *2
            {
                return res.status(400).json({
                    error: "File size too big!"
                });
            }
            // data came from product in models mention fullpath of our file and passing to form that is parameter which is in 28 line
            product.photo.data = fs.readFileSync(file.photo.path); // technically this two lines saves in database
            product.photo.contentType = file.photo.type; //10 Section - 03 @12:28 type jpeg or peg
        }
        // console.log(product);


        // Save to the DB
        product.save((err, product) => {
            if (err) {
                res.status(400).json({
                    error: "Updation of product failed"
                });
            }
            res.json(product);
        });
    });
};


// product listing

exports.getAllProducts = (req, res) => {
    // let limit = 8 // it will show 8 product of list
    let limit = req.query.limit ? parseInt(req.query.limit) : 8;
    let sortBy = req.query.sortBy ? req.query.sortBy : "_id"; // default sort by id
    Product.find()
        .select("-photo") // it will not select the photo since it effect the performance
        .populate("category")
        .sort([[sortBy, "asc"]])
        .limit(limit)
        .exec((err, products) => {
            if (err) {
                return res.status(400).json({
                    error: "No Product Found"
                });
            }
            res.json(products);
        });
};

// we have middleware stock and sold changes based on the purchase
//https://mongoosejs.com/docs/api.html#model_Model.bulkWrite
exports.updateStock = (req, res, next) => {
    let myOperations = req.body.order.products.map(prod => {
        return {
            updateOne: {
                filter: { _id: prod._id }, // find the product with the id
                update: { $inc: { stock: -prod.count, sold: +prod.count } } // update the stock and also sold one
            }
        };
    });

    // eslint-disable-next-line no-unused-vars
    Product.bulkWrite(myOperations, {}, (err, products) => {
        if (err) {
            return res.status(400).json({
                error: "Bulk operation failed"
            });
        }
    });
    next();

};

exports.getAllUniqueCategories = (req, res) => {
    Product.distinct("category", {}, (err, category) => {
        if (err) {
            return res.status(400).json({
                error: "No category found"
            });
        }
        res.json(category);
    });
};

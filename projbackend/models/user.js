/* eslint-disable quotes */
var mongoose = require("mongoose");
const crypto = require('crypto');
//import { v1 as uuidv1 } from 'uuid';
const uuidv1 = require('uuid/v1');



//var Schema = mongoose.Schema;

var userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true, // always come in string or else it will throw an error.
        maxlength: 32, // number of character in name which is long enough
        trim: true, // It will trim if we found extra space
    },
    lastname: {
        type: String,
        maxlength: 32,
        trim: true,
    },
    email: {
        type: String,
        trim: true,
        required: true,
        unique: true
    },

    userinfo: {
        type: String,
        trim: true
    },

    // TODO: Come back here
    encry_password: { // encrpted password
        type: String,
        required: true
    },
    salt: String,
    role: { // privilege like admin, customer, customer care
        type: Number,
        default: 0 // only regular user
    },

    purchase: {
        type: Array, // purchases are stored in array 
        default: [] // empty no purchases has been done
    }

}, { timestamps: true });

// virtual is same as userSchema.method but we are creatind in virtual!

// NOTE: Crypto is used to encrypt the plain password. UUID is used to get unique string. As crypto need some
// unique string to encrypt the password.
userSchema.virtual("password")
    .set(function (password) { // it will set
        this._password = password;// _password it's a private variable in the javascript!
        this.salt = uuidv1(); // uuid is used to get unique string 
        this.encry_password = this.securePassword(password); //  from securepassword method I am getting encrypted password so it's assigned
    })
    .get(function () { // take this field back! 
        return this._password;
    });



userSchema.methods = {

    autheticate: function (plainpassword) { // to check once he register into website
        return this.securePassword(plainpassword) === this.encry_password;
    },

    securePassword: function (plainpassword) { // passing a plain password and need to return a encrypted password
        if (!plainpassword) return "";
        // encrypt the password if it has
        try {
            return crypto.createHmac('sha256', this.salt)
                .update('plainpassword')
                .digest('hex');

        } catch (err) {
            return ""; // empty password cannot be store!
        }
    }
};

module.exports = mongoose.model("User", userSchema); // userSchema is calling from User instead of User


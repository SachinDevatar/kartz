// See FlipKart order page to get good idea from flipkart.com

const mongoose = require("mongoose");

const { ObjectId } = mongoose.Schema;

const ProductCartSchema = new mongoose.Schema({ //  product already existed in website and so we are adding same
    // in order page again look order page in filpkart
    product: {
        type: ObjectId,
        ref: "Product"
    },
    name: String,
    count: Number,
    price: Number
});


const ProductCart = mongoose.model("ProductCart", ProductCartSchema);

const OrderSchema = new mongoose.Schema({
    products: [ProductCartSchema], // product inside the cart so it's stored in array
    transaction_id: {},
    amount: { type: Number },
    address: String,
    status: {
        type: String,
        default: "Recieved",
        // Airplane example where you have only 3 option: ailes, middle and window no corner so in same order we need to restict it. 
        enum: ["Cancelled", "Delivered", "shipped", "processing", "Recieved"]
    },
    updated: Date,
    user: {
        type: ObjectId,
        ref: "User" // we are populate in user.js controller
    }
}, { timestamps: true }
);

// throw this two things :
const Order = mongoose.model("Order", OrderSchema);

module.exports = { Order, ProductCart };
const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema; // pulling category from category schema using object Id. which you 
// see in the Robo3T

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: true,
        maxlength: 32
    },

    description: {
        type: String,
        trim: true,
        required: true,
        maxlength: 2000
    },
    price: {
        type: Number,
        required: true,
        maxlength: 32,
        trim: true
    },
    category: {
        type: ObjectId, // we need to pass objectID only from Robo3T to see the category see section 10- 05- @5:37
        ref: "Category", // from category we are pulling the data through reference
        required: true,
    },
    stock: {
        type: Number // No of T shirt
    },
    sold: {
        type: Number,
        default: 0 // when you are not selling anything it's empty
    },
    photo: {
        data: Buffer, // photos are stored in Buffer!
        contentType: String
    }// many ways to fetch the photo we should not dumb all the photos in DB instead we 
    //can add it in S3 and then call from it using address

    // in Firebase just put in folder and pull out a reference
}, { timestamps: true });

module.exports = mongoose.model("Product", productSchema);
const express = require("express");
const router = express.Router();

// params concepts taking from user controller
const { getUserById, getUser, updateUser, userPurchaseList } = require("../controllers/user");// getting getUserByID from user.js controller

// eslint-disable-next-line no-unused-vars
const { isSignedIn, isAuthenticated, isAdmin } = require("../controllers/auth");

router.param("userId", getUserById);
// :something that will be interpreated as userID this method automatically populate request.profile object 
//from userobject that is coming from database

router.get("/user/:userId", isSignedIn, isAuthenticated, getUser);
// router.get("/users", getAllUsers); to get all users from DB

router.put("/user/:userId", isSignedIn, isAuthenticated, updateUser);
router.get("/orders/user/:userId", isSignedIn, isAuthenticated, userPurchaseList);


module.exports = router;


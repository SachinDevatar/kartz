const express = require("express");
const router = express.Router();


const { getCategoryById, createCategory, getCategory, getAllCategory, updateCategory, removeCategory } = require("../controllers/category");
const { isSignedIn, isAuthenticated, isAdmin } = require("../controllers/auth")
const { getUserById } = require("../controllers/user")


// params
router.param("userId", getUserById); // 
router.param("categoryId", getCategoryById); // it will get particular id of customer


// actual routers goes here

//create routes
router.post("/category/create/:userId", isSignedIn, isAuthenticated, isAdmin, createCategory); // see section 9 4th at 1:15 to see how it works

//read routes that is get
router.get("/category/:categoryId", getCategory);
router.get("/categories", getAllCategory);

// update
router.put("/category/:categoryId/:userId", isSignedIn, isAuthenticated, isAdmin, updateCategory);

//delete 
router.delete("/category/:categoryId/:userId", isSignedIn, isAuthenticated, isAdmin, removeCategory);



module.exports = router;
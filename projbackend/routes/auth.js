/* eslint-disable no-unused-vars */
var express = require("express");
var router = express.Router();
const { check, validationResult } = require("express-validator"); // here checks are used
const { signout, signup, signin, isSignedIn } = require("../controllers/auth");

// Good practice to do in controller!
// const signout = (req, res) => {
//     // res.send("user signout success");
//     res.json({ // Json response
//         message: "User signout"
//     });
// };

// Post route of Signup
router.post("/signup", [
    check("name", "name should be atleast 3 char").isLength({ min: 3 }), // test message is added next to "name" if it doesn't work then go for .withmessage
    check("email", "email is required").isEmail(),
    check("password", "password should be atleast 3 char").isLength({ min: 3 })
], signup);


// Post route of Signin/login
router.post("/signin", [
    // no need to check name when he login
    check("email", "email is required").isEmail(),
    check("password", "password field is required").isLength({ min: 1 })
], signin);




router.get("/signout", signout);

// router.get("/testroute", isSignedIn, (req, res) => { // kind of middle ware isSignedIn
//     // res.send("A projected route");  // next keyword is required to have it but as expressjwt consists next already so we are not using it 
//     // it's highly recommended to use next when we use middle ware!!

//     res.json(req.auth);

// });


module.exports = router;
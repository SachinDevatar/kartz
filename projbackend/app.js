/* eslint-disable no-unused-vars */
// First launch of the application will take place here and it's like a entry point to the execution!

// see dotenv in npm webpage
require("dotenv").config();

const mongoose = require("mongoose");
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const cors = require("cors");

// my routes
const authRoutes = require("./routes/auth");
const userRoutes = require("./routes/user");
const categoryRoutes = require("./routes/category");
const productRoutes = require("./routes/product");
const orderRoutes = require("./routes/order");

//mongoose.connect('mongodb://localhost:27017/test', { useNewUrlParser: true });
// process is the thing in which it will attach all the enviromental variable

// DB Connections
mongoose.connect(process.env.DATABASE, { // this parameter will keep us alive to run database
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
}).then(() => {
    console.log("DB CONNECTED");
}).catch(
    console.log("DB Crashed!!"));
// connection chaining ! important in Javascript which is above


// Middlewares
app.use(bodyParser.json());
app.use(cookieParser()); // puts or deletes a value into the cookie
app.use(cors());


//My Routes will must be go throw it /api
app.use("/api", authRoutes); // user has to visit /api if he wants to visit our system
app.use("/api", userRoutes); -
    app.use("/api", categoryRoutes);
app.use("/api", productRoutes);
app.use("/api", orderRoutes);



//PORT
const port = process.env.PORT || 8000; // see .env to hide essential info and port number we are using .env see enviroment variable video


// Starting a Server and it's listening in port 8000. //
app.listen(port, () => {
    console.log(`app is running at ${port}`);
});

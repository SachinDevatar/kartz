# kArtZ

**About the Project:**

- E-Commerce website Like Amazon where User can purchase any item from A to Z with dummy payment processing using MERN (MongoDB, Express, React, and Node.js) Stack


- This experience is absolutely amazing and it gives me loads of must-have knowledge and confidence to start off my next big project using the latest Technology that is MERN Stack.

- I have also used the latest React Hooks which is an absolute joy. The code will be so much simplified and readable, that I didn't believe it.

- The Node API will follow the MVC Pattern. By doing a lot of research I can say the codebase will be so clean and elegant, you will be absolutely loving it. 

- With the making of this application, I get introduced to Braintree (A PayPal Company) for handling Payments. 

- Braintree is hands down the best payment gateway you could possibly use in a production site for both PayPal and Credit Card payments.

**Some snippet of features are :**

- Wrote Functional Components with React Hooks
- Handling File Upload
- Implemented Authentication based on JWT
- Create a Secure REST API in Node.js
- Built Admin and User Dashboard
- Built Scalable React App with Proper Layouts and Routes Integrated Braintree - PayPal Company for Payment Processing

It has a lot more features than above mentioned.

**Technology Stack:  Javascript, ReactJS, NodeJS, ExpressJS, MongoDB, BitBucket, AWS(Amazon Web Service).**
